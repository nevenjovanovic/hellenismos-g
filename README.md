# README #

A Greek Reader for the course Grčka morfologija 1 (Greek morphology 1), 2018/19, University of Zagreb, Croatia, Faculty of Humanities and Social Sciences, [Department of Classical Philology](http://www.ffzg.unizg.hr/klafil/). The reader and associated exercises are in Croatian (and Greek).

The reader will eventually contain 60 brief extracts from original Greek texts, from Herodotus to Plotinus, with ample commentary and vocabulary exercises.

The commentary is keyed to the standard Croatian school grammar of Greek, August Musić, Nikola Majnarić, *Gramatika grčkoga jezika*, Zagreb (many reprints).

The texts were selected and commented by the following members of the Department (in alphabetical order):  Irena Bratičević, Nina Čengić, Neven Jovanović, Vlado Rezar, Petra Šoštarić, Ninoslav Zubović.

This directory was prepared by [Neven Jovanović](http://orcid.org/0000-0002-9119-399X).

# License #

[CC Attribution 4.0 International](https://bitbucket.org/nevenjovanovic/hellenismos/src/default/LICENSE.md)

# Exported to Git #

Because Bitbucket is removing its Mercurial support, this repository has been exported to Git (using [hg-fast-export](https://github.com/frej/fast-export)) on 2018-08-24.

# How to use? #

* The typeset (compiled) text (PDF) is here:  [Part One](https://bitbucket.org/nevenjovanovic/hellenismos/src/default/citanka/grc_morf_01_citanka_2018-1.pdf), [Part Two](https://bitbucket.org/nevenjovanovic/hellenismos/src/default/citanka/grc_morf_02_citanka_2018-1.pdf)
* The Anki deck (.apkg), ready for import, is [here](https://bitbucket.org/nevenjovanovic/hellenismos/src/default/rijeci/GrcMorf1a.apkg)
* The [LaTeX](https://www.latex-project.org/) source is in [citanka](https://bitbucket.org/nevenjovanovic/hellenismos/src/default/citanka/)
* The [Anki](https://apps.ankiweb.net/docs/manual.html) decks (and source as plain text files) are in [rijeci](https://bitbucket.org/nevenjovanovic/hellenismos/src/default/rijeci/)

# Sources #

The digital texts for this reader were selected from the Perseus Digital Library and Open Greek and Latin repositories.

Vocabulary for the exercises was prepared and selected using the [Perseids Treebank Annotation](http://www.perseids.org/tools/arethusa/app/#/) framework, the [Dickinson College Greek Core Vocabulary](http://dcc.dickinson.edu/vocab/core-vocabulary) lists, and a number of XQuery scripts.

# Who do I talk to? #

* Repo owner: [Neven Jovanović](https://bitbucket.org/nevenjovanovic)

